build: 
	cc main.c -o pika
	echo /gophermap | ./pika

debug: 
	cc -ggdb main.c -o pika

memcheck: debug
	valgrind --track-origins=yes --tool=memcheck --leak-check=yes ./pika

logo: 
	convert -background '#ffffe8' -fill '#424242' -pointsize 72 label:pika logo.png

prod: 
	cc main.c -o pika
