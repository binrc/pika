#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>

/* UNCOMMENT FOR LINUX */
// #include <linux/limits.h>
// ssize_t _XOPEN_PATH_MAX = PATH_MAX;

// configure here
ssize_t ROOTLEN = 20;
char *DOCROOT = "/opt/pika/docroot";

// enable log, breaks pledge/unveil, useful for debugging only
int LOG = 0;
char *LOGFILE = "/opt/pika/access.log";

int main(){
	/* COMMENT FOR LINUX */
	if(unveil(DOCROOT, "r") == -1){
		exit(1);
	}
	if(pledge("stdio rpath", NULL) == -1){
		exit(1);
	}
	/* */


	// input
	ssize_t maxpath = _XOPEN_PATH_MAX * 8;
	char *request;
	if((request = (char *) malloc(maxpath)) == NULL){
		exit(1);
	}

	fgets(request, maxpath, stdin);
	request[strcspn(request, "\r\n")] = 0;	// strip newlines

	if(!strcmp(request, "")){		// redirect empty request
		strcat(request, "/gophermap");
	}

	// output
	ssize_t fullreqlen = maxpath + ROOTLEN;
	char *fullreq;
	if((fullreq = (char *) malloc(fullreqlen)) == NULL){
		exit(1);
	}
	strcpy(fullreq, DOCROOT);
	strcat(fullreq, request);

	if(strstr(request, "../") != NULL || strstr(request, "..%2f") != NULL){	// sanitize
		printf("3Nice try\r\n");
		goto free;
	}


	FILE *fp;
	if((fp = fopen(fullreq, "rb")) == NULL){
		printf("3No such file or directory\r\n");
	} else {
		// buffered IO
		fseek(fp, 0, SEEK_END);
		ssize_t fs = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		int *buf;
		if((buf = malloc(fs + 1)) == NULL){
			exit(1);
		}
		fread(buf, fs, 1, fp);
		fclose(fp);

		fwrite(buf, fs, 1, stdout);
		fflush(stdout);	
		free(buf);

	}

	if(LOG){
		if((fp = fopen(LOGFILE, "a")) == NULL){
			//printf("3Log file broked\n"); // don't tell the user anything
			goto free;
		} else {
			fprintf(fp, "%s\t%s\n", request, fullreq);
		}
		fclose(fp);
	}

free: 
	free(fullreq);
	free(request);
}
