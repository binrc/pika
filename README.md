# What is pika? 

A gopher server that sits behind inetd. I like pikas and they look like a mousier version of a gopher. I have grown tired of the "$LANG+$PROTOCOL" naming scheme after seeing "RUST+$PROTOCOL" style naming conventions littering my RSS feeds over the past few months. Pika is a more unique name than "Cgopher" anyway. 

# Server setup
Only tested on OpenBSD. Running pika behind inetd as the `daemon` user. You can probably do this in production. 

```
$ make prod
$ cat inetd.conf >> /etc/inetd.conf
$ touch access.log
$ chown root:daemon access.log
$ chmod g+w ./access.log
$ rcctl restart inetd
```

# Modifying options

In order to modify server options, you will edit `main.c` then recompile. Change the global vars to suit your configuration. You will also need to edit `/etc/inetd.conf` to respect your new configuration. 

The `/opt/` directory was chosen as a sane default because that's what the `/opt` directory is for: programs that are neither part of the base system nor provided by a package manager. 

This is default: 
```C
// configure here
ssize_t ROOTLEN = 20;
char *DOCROOT = "/opt/pika/docroot";

int LOG = 1;
char *LOGFILE = "/opt/pika/access.log";
```

Here is an insane modified version you should not use. It exists purely as an example. Be sure to bump up your `ROOTLEN` variable if your path is long. Toggling the `LOG` variable disables logging. 
```C
// configure here
ssize_t ROOTLEN = 25;
char *DOCROOT = "/root/bin/pika/docroot";

int LOG = 1;
char *LOGFILE = "/root/bin/pika/access.log";
```

# Building on linux

Read source code, uncomment 2 lines. I don't think you guys have inetd anymore anyway so it's kind of a ridiculous thing for me to even consider testing against.

# Missing essential security features

- no whitelist support
- it ~~leaks~~ currently does not leak

# gophermap example

pika does absolutely no heavy lifting for the user. It's essentially a modified `cat(1)` program. The gophermap must be entirely complete or clients will be unable to process it. An example gophermap looks like: 

```
istandard options: 
0gopher rfc	/rfc1436.txt	192.168.122.249	70
1sub directory	/sub/gophermap	192.168.122.249	70
i
inonstandard options
Ipika logo	/logo.png	192.168.122.249	70
i
iother options are largely irrelevent in 1970+($YEAR-1970)
```

This format is: 
```
itemType	userDisplayString	path	host	port
```

All fields are required as pika does absolutely no heavy lifting for the admin. 

# TODO

- ~~[X] whitelist support~~ add a sanitizer because gopher is simple
- [X] buffered IO
- ~~[X] filetype awareness~~ treating everything like binary fixes this
- [X] remove leaks

> ==132389== All heap blocks were freed -- no leaks are possible

- ~~[X] binary file support~~


# ISSUES

> THIS SERVER IS BROKEN!!!

Your client is probably bad, get a better client. I tested with bombadillo, lagrange, castor, cgo, gophie, and lynx. Bombadillo, lynx, cgo, and gophie seem to work just fine. Lagrange and castor claim to support gopher but this is not true. Curiously enough, curl also has (some) gopher support. 

Your gophermap is probably bad, write a better gophermap. All gophermaps are processed on the client. Try a variety 
